#!/bin/bash

docker run -d --name postgres -p 5432:5432 -v ~/docker_vol/db_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD=123456 -e POSTGRES_USER=postgres -e POSTGRES_DB=postgres postgres:14-alpine
