DROP TABLE if EXISTS ethereum.t_transaction;
DROP TABLE if EXISTS ethereum.t_block;
DROP SCHEMA if EXISTS ethereum;

CREATE SCHEMA ethereum;

CREATE TABLE ethereum.t_block (
  i_number bigint PRIMARY KEY,
  s_hash varchar(128) NOT NULL UNIQUE,
  s_parent_hash varchar(128) NOT NULL,
  s_nonce varchar(32) NOT NULL,
  s_sha3_uncles varchar(128) NOT NULL,
  s_logs_bloom text,
  s_transaction_root varchar(128),
  s_state_root varchar(128) NOT NULL,
  s_miner varchar(128) NOT NULL,
  i_difficulty bigint NOT NULL,
  i_total_difficulty bigint NOT NULL,
  i_size bigint NOT NULL,
  i_gas_limit bigint NOT NULL,
  i_gas_used bigint NOT NULL,
  dt_timestamp TIMESTAMP NOT null,
	dt_created_at TIMESTAMP NOT null default CURRENT_TIMESTAMP
);

CREATE TABLE ethereum.t_transaction (
  id bigserial PRIMARY KEY,
  i_block_number bigint NOT NULL,
  s_block_hash varchar(128) NOT NULL,
  s_from varchar(128) NOT NULL,
  s_to varchar(128),
  i_gas bigint NOT NULL,
  i_gas_price bigint NOT NULL,
  s_hash varchar(128) NOT NULL,
  s_input text,
  i_nonce bigint NOT NULL,
  i_transaction_index bigint NOT NULL,
  dec_value decimal(100,0) NOT NULL,
  i_type bigint NOT NULL,
  s_v varchar(128) NOT NULL,
  s_r varchar(128) NOT NULL,
  s_s varchar(128) NOT NULL,
	dt_created_at TIMESTAMP NOT null default CURRENT_TIMESTAMP,
  CONSTRAINT uidx_t_transaction_s_block_hash_s_hash UNIQUE (s_block_hash, s_hash)
);
