Prerequiesits
---
1. A postgres DB should be running at localhost:5432. Check ```infra``` for scripts to run it on docker.


2. A ethereum client is running on ```Alchemy```.


3. Secrets are stored in ```secrets.json``` in the project root directory, and it looks like this:
```
{
  "mnemonic": "abc cde ...",
  "alchemyApiKey": "ABCDEF...",
  "contractAddress": "0xADSF..",
  "account": "0xASEDF..."
}
```


Development and Testing
---

To install libraries:
```
npm install
```

To build the project:
```
npm run build
```

To run the project:
```
npm run start
```
