import EthereumPoller from '../src/ethereum-poller';

var assert = require('assert');
const poller = new EthereumPoller();

describe('EthereumPoller', function() {
  describe('#getLatestBlockNumber()', function() {
    it('should return a block number', async function() {
      const blockNumber = await poller.getLatestBlockNumber();
      // console.log(blockNumber);
      assert(blockNumber > 0);

    });

    it('the block number of latest block should be the same as what returned by getLatestBlockNumber()', async function() {
      const blockNumber = await poller.getLatestBlockNumber();
      const block = await poller.getLatestBlock();
      // console.log(block.number);
      assert.equal(blockNumber, block.number);
    });
  });

  // describe('#getLatestBlock()', function() {
  //   it('the block number of latest block should be the same as what returned by getLatestBlockNumber()', async function() {
  //     const block = await poller.getLatestBlock();
  //     // console.log(block.number);
  //     assert.equal(blockNumber, block.number);
  //   });
  // });


  
});