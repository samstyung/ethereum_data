import { QueryResult, Pool } from "pg";

export default class Database {
  private pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: '123456',
    port: 5432,
  });

  public async getBlockCountInDateRange(from: string, to: string): Promise<number>{
    const sql = 'select count(1) from ethereum.t_block tb where dt_timestamp between $1 and $2';
    const params = [
      from,
      to
    ];

    let count = -1;

    await this.pool
    .query(sql, params)
    .then(result => {
      console.log(result.rows[0]);
      count = result.rows[0].count;
    });

    // console.log(count);
    return count;
  }

  public async getGasUsedInDateRange(from: string, to: string): Promise<number>{
    const sql = 'select sum(i_gas_used) as sum from ethereum.t_block tb where dt_timestamp between $1 and $2';
    const params = [
      from,
      to
    ];

    let sum = -1;

    await this.pool
    .query(sql, params)
    .then(result => {
      console.log(result.rows[0]);
      sum = result.rows[0].sum;
    });

    // console.log(count);
    return sum;
  }

  public insertBlock(block: any) {
    const sql = 'INSERT INTO ethereum.t_block ('
    + ' i_number'
    + ', s_hash'
    + ', s_parent_hash'
    + ', s_nonce'
    + ', s_sha3_uncles'
    + ', s_logs_bloom'
    + ', s_transaction_root'
    + ', s_state_root'
    + ', s_miner'
    + ', i_difficulty'
    + ', i_total_difficulty'
    + ', i_size'
    + ', i_gas_limit'
    + ', i_gas_used'
    + ', dt_timestamp'
    + ') VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, to_timestamp($15)) ON CONFLICT DO NOTHING';

    const params = [
      block.number, 
      block.hash, 
      block.parentHash, 
      block.nonce, 
      block.sha3Uncles, 
      block.logsBloom, 
      block.transactionRoot, 
      block.stateRoot, 
      block.miner, 
      block.difficulty, 
      block.totalDifficulty, 
      block.size, 
      block.gasLimit, 
      block.gasUsed, 
      block.timestamp
    ];

    // console.log(params);

    this.pool.query(sql, params,
      (error, result) => {
        if (error) {
          throw error;
        }
        // console.log(result);
      }
    );

    // pool.query(
    //   "SELECT * from ethereum.t_block",
    //   (error, result) => {
    //     if (error) {
    //       throw error;
    //     }
    //     console.log(result);
    //     pool.end();
    //   }
    // );
  }

  public insertTransaction(txn: any) {
    // console.log(txn);

    const sql = 'INSERT INTO ethereum.t_transaction ('
    + ' s_block_hash'
    + ', i_block_number'
    + ', s_from'
    + ', s_to'
    + ', i_gas'
    + ', i_gas_price'
    + ', s_hash'
    + ', s_input'
    + ', i_nonce'
    + ', i_transaction_index'
    + ', dec_value'
    + ', i_type'
    + ', s_v'
    + ', s_r'
    + ', s_s'
    + ') VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) ON CONFLICT DO NOTHING';

    const params = [
      txn.blockHash, 
      txn.blockNumber, 
      txn.from, 
      txn.to, 
      txn.gas, 
      txn.gasPrice, 
      txn.hash, 
      txn.input, 
      txn.nonce, 
      txn.transactionIndex, 
      txn.value, 
      txn.type, 
      txn.v, 
      txn.r, 
      txn.s
    ];

    // console.log(params);

    this.pool.query(sql, params,
      (error, result) => {
        if (error) {
          throw error;
        }
        // console.log(result);
      }
    );

  }
}