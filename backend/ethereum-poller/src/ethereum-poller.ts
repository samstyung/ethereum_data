const { createAlchemyWeb3 } = require("@alch/alchemy-web3");
import Database from './database';
import { ethers } from "ethers";
// const Contract = require('web3-eth-contract');
const { alchemyApiKey, mnemonic, contractAddress, account } = require('../../../secrets.json');

class EthereumPoller {
  // private web3 = new Web3('http://localhost:8545');

  // public rinkeby light node
  // private web3 = new Web3('https://rinkeby-light.eth.linkpool.io');

  private url = 'https://eth-rinkeby.alchemyapi.io/v2/'+alchemyApiKey;
  private web3 = createAlchemyWeb3(this.url);
  private db = new Database();
  private wallet = ethers.Wallet.fromMnemonic(mnemonic);
  private contractJson = require("../../../smart_contract/artifacts/contracts/eth_data.sol/eth_data.json");
  private contractAbi = this.contractJson.abi;

  // wrapper of web3 API
  public async getAccountBalance(): Promise<number> {
    return await this.web3.eth.getBalance(account);
  }

  // wrapper of web3 API
  public async getLatestBlockNumber(): Promise<number> {
    return await this.web3.eth.getBlockNumber();
  }

  // wrapper of web3 API
  public async getBlock(blockNumber: number): Promise<any> {
    return await this.web3.eth.getBlock(blockNumber, true);
  }

  // wrapper of web3 API
  public async getLatestBlock(): Promise<any> {
    return await this.web3.eth.getBlock('latest', true);
  }

  // retrieve block count from smart contract
  public async getBlockCount(date: string): Promise<number> {
    const provider = new ethers.providers.JsonRpcProvider(this.url);
    const signer = provider.getSigner(account);
    const contract = new this.web3.eth.Contract(this.contractAbi, contractAddress);
    const blockCount = await contract.methods.blockCountOn(date).call();
    console.log('blockCount', blockCount);
    return blockCount;
  }

  // retrieve gas used from smart contract
  public async getGasUsed(date: string): Promise<number> {
    const provider = new ethers.providers.JsonRpcProvider(this.url);
    const signer = provider.getSigner(account);
    const contract = new this.web3.eth.Contract(this.contractAbi, contractAddress);
    const gasUsed = await contract.methods.gasUsedOn(date).call();
    console.log('gasUsed', gasUsed);
    return gasUsed;
  }

  // update block count on smart contract
  public async updateBlockCount(date: string, blockCount: number) {
    // start storing onto smart contract
    const provider = new ethers.providers.JsonRpcProvider(this.url);
    const signer = provider.getSigner(account);
    const contract = new this.web3.eth.Contract(this.contractAbi, contractAddress);

    const nonce = await this.web3.eth.getTransactionCount(contractAddress, 'latest')+1; // get latest nonce
    console.log('nonce', nonce);

    // Create the transaction
    const tx = {
      'from': this.wallet.address,
      'to': contractAddress,
      'nonce': nonce,
      'gas': 0, 
      'data': contract.methods.updateBlockCount(date, blockCount).encodeABI()
    };

    const gasEstimate = await this.web3.eth.estimateGas(tx);
    console.log('gasEstimate', gasEstimate);

    tx['gas'] = gasEstimate;

    console.log('tx', tx);

    // Sign the transaction
    this.web3.eth.accounts.signTransaction(tx, this.wallet.privateKey)
    .then((signedTx: any) => {
      let rawTransaction = '';
      if (signedTx.rawTransaction != undefined) {
        rawTransaction = signedTx.rawTransaction;
      }
      console.log('rawTransaction', rawTransaction);

      this.web3.eth.sendSignedTransaction(rawTransaction, function(err: any, hash: any) {
        if (!err) {
          console.log("The hash of your transaction is: ", hash, "\n Check Alchemy's Mempool to view the status of your transaction!");
        } else {
          console.log("Something went wrong when submitting your transaction:", err)
        }
      });
    }).catch((err: any) => {
      console.log("Promise failed:", err);
    });
  }

  // update gas used on smart contract
  public async updateGasUsed(date: string, gasUsed: number) {
    // start storing onto smart contract
    const provider = new ethers.providers.JsonRpcProvider(this.url);
    const signer = provider.getSigner(account);
    const contract = new this.web3.eth.Contract(this.contractAbi, contractAddress);

    const nonce = await this.web3.eth.getTransactionCount(contractAddress, 'latest')+1; // get latest nonce
    console.log('nonce', nonce);

    // Create the transaction
    const tx = {
      'from': this.wallet.address,
      'to': contractAddress,
      'nonce': nonce,
      'gas': 0, 
      'data': contract.methods.updateGasUsed(date, gasUsed).encodeABI()
    };

    const gasEstimate = await this.web3.eth.estimateGas(tx);
    console.log('gasEstimate', gasEstimate);

    tx['gas'] = gasEstimate;

    console.log('tx', tx);

    // Sign the transaction
    this.web3.eth.accounts.signTransaction(tx, this.wallet.privateKey)
    .then((signedTx: any) => {
      let rawTransaction = '';
      if (signedTx.rawTransaction != undefined) {
        rawTransaction = signedTx.rawTransaction;
      }
      console.log('rawTransaction', rawTransaction);

      this.web3.eth.sendSignedTransaction(rawTransaction, function(err: any, hash: any) {
        if (!err) {
          console.log("The hash of your transaction is: ", hash, "\n Check Alchemy's Mempool to view the status of your transaction!");
        } else {
          console.log("Something went wrong when submitting your transaction:", err)
        }
      });
    }).catch((err: any) => {
      console.log("Promise failed:", err);
    });
  }

  // poll the latest block and store block/transactions info into DB.
  public poll() {
    this.getLatestBlock().then(block => {
      console.log('block number:', block.number);
      console.log('storing block data to DB');
      this.db.insertBlock(block);
      if (block.transactions != null) {
        console.log('transaction:', block.transactions.length);
        console.log('storing transaction data to DB');
        for (var txn of block.transactions) {
          this.db.insertTransaction(txn);
        }
      }
    });
  }

  public async runDailyJob() {
    const currenctDateTime = new Date();
    const currentHour = currenctDateTime.getHours();
    const currentMinute = currenctDateTime.getMinutes();

    console.log(currentHour+':'+currentMinute);

    // do it only when 00:01
    if (currentHour != 0 || currentMinute != 1) {
      console.log('skip runDailyJob()');
      return;
    }

    console.log('start runDailyJob()');

    // get block count from DB
    const minus1DateTime = new Date(currenctDateTime.getTime() - 24*60*60*1000);
    const minus2DateTime = new Date(currenctDateTime.getTime() - 2*24*60*60*1000);
    // const currenctDateStr = ''+currenctDateTime.getFullYear()+currenctDateTime.getMonth()+currenctDateTime.getDay();
    const minus1DateStr = ''+minus1DateTime.getFullYear()+minus1DateTime.getMonth()+minus1DateTime.getDay();
    const minus2DateStr = ''+minus2DateTime.getFullYear()+minus2DateTime.getMonth()+minus2DateTime.getDay();

    const blockCountOnContract = await this.getBlockCount(minus1DateStr);
    if (!blockCountOnContract || blockCountOnContract < 0) {
      const blockCount = await this.db.getBlockCountInDateRange(minus2DateStr, minus1DateStr); 
      console.log('blockCount', blockCount);
      if (blockCount > 0) {
        await this.updateBlockCount(minus1DateStr, blockCount);
      }
    }

    const gasUsedOnContract = await this.getGasUsed(minus1DateStr);
    if (!gasUsedOnContract || gasUsedOnContract < 0) {
      const gasUsed = await this.db.getGasUsedInDateRange(minus2DateStr, minus1DateStr); 
      console.log('gasUsed', gasUsed);
      if (gasUsed > 0) {
        await this.updateGasUsed(minus1DateStr, gasUsed);
      }
    }

    console.log('end runDailyJob()');
  }
}

export default EthereumPoller;

