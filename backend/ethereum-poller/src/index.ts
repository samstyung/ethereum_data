import EthereumPoller from '../src/ethereum-poller';

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const poller = new EthereumPoller();

// async function main() {
//   const balance = await poller.getAccountBalance();
//   console.log('balance', balance);
// }

// async function main() {
//   const gasUsed = await poller.getGasUsed('20211230');
//   const blockCount = await poller.getBlockCount('20211230');
// }

// async function main() {
//   await poller.updateGasUsed('20211230', 123);
//   await poller.updateBlockCount('20211230', 123);
// }

async function main() {
  while (true) {
    await poller.runDailyJob();  
    poller.poll();
    const balance = await poller.getAccountBalance();
    console.log('account balance', balance);
    await delay(5000);
  }
}

main();
