//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";

contract eth_data is Ownable {
	mapping(string => uint256) public gasUsed;
  mapping(string => uint256) public blockCount;

	function gasUsedOn(string memory date) public view returns(uint256){
		return gasUsed[date];
	}
	
	function blockCountOn(string memory date) public view returns(uint256){
		return gasUsed[date];
	}
	
	function updateGasUsed(string memory date, uint256 value) public onlyOwner
	returns(bool){
		require(value>=0);
		gasUsed[date]=value;
		return true;
	}

	function updateBlockCount(string memory date, uint256 value) public onlyOwner
	returns(bool){
		require(value>=0);
		blockCount[date]=value;
		return true;
	}
}