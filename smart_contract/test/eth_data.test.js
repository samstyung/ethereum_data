const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("EthData", function () {
  describe("#updateGasUsed()", function () {
    it("Should return the updated value", async function () {
      const date = '20211230'
      const gasUsed = 123

      const EthData = await ethers.getContractFactory("eth_data");
      const contract = await EthData.deploy();
      await contract.deployed();
      await contract.updateGasUsed(date, gasUsed)

      expect(await contract.gasUsedOn(date)).to.equal(gasUsed);
    });
  });
});
