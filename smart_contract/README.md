Development and Testing
---

This project is using Hardhat framework to build and test.

To install hardhat:
```
npm install --save-dev hardhat
```

The smart contract ```eth_data.sol``` is stored in directory ```contracts```.

Secrets are stored in ```secrets.json``` in the project root directory. And it looks like this:
```
{
  "mnemonic": "abc cde ...",
  "alchemyApiKey": "ABCDEF...",
  "contractAddress": "0xADSF..",
  "account": "0xASEDF..."
}
```

To run a local ethereum node:
```
npx hardhat node
```

To compile the smart contract:
```
npx hardhat compile
```

To deploy the smart contract to the local etherum node:
```
npx hardhat run --network localhost scripts/deploy.js
EthData deployed to: 0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0
```

To run unit tests:
```
npx hardhat test
```

To interact with the deployed smart contract:
```
npx hardhat console --network localhost
```

Test the smart contract:
```
> const EthData = await ethers.getContractFactory('eth_data');
undefined
> const contract  = await EthData.attach('0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0')
undefined
> await contract.updateGasUsed('20211230', 123)
{
  hash: '0x2f6e5802712fd3a129db576fcb6e41d9d5e3d0801ae9bda0ce4944ecdef49456',
  type: 2,
  accessList: [],
  blockHash: '0xedba159b38862171fd73e2c13d315a6cbde2a3f37d219809e93bfa35e69922ea',
  blockNumber: 4,
  transactionIndex: 0,
  confirmations: 1,
  from: '0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266',
  gasPrice: BigNumber { value: "594451326" },
  maxPriorityFeePerGas: BigNumber { value: "0" },
  maxFeePerGas: BigNumber { value: "752352459" },
  gasLimit: BigNumber { value: "47968" },
  to: '0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0',
  value: BigNumber { value: "0" },
  nonce: 3,
  data: '0x70f2bacd0000000000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000000000000000000000000000007b00000000000000000000000000000000000000000000000000000000000000083230323131323330000000000000000000000000000000000000000000000000',
  r: '0x0aab1f26d0702b2659665a73d7ffe44840d9e9d4a9412bba89275459d297ee96',
  s: '0x1ff9089d6ad67cc5c39127fe98a00ee881d168633e1ce411714688815f55a2f2',
  v: 0,
  creates: null,
  chainId: 31337,
  wait: [Function (anonymous)]
}
> await contract.gasUsedOn('20211230')
BigNumber { value: "123" }
> 
```

Test on TESTNET - Rinkeby
---

To deploy to Rinkeby
```
npx hardhat run --network rinkeby scripts/deploy.js
```

Note: The contract has already been deployed to Rinkeby with address
```
0x83f3661BE96767F1c554f0885B26330083c08ed1
```

To interact with the deployed smart contract:
```
npx hardhat console --network rinkeby
```
